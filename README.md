# lanbilling

<table style="height: 783px; width: 1028px;">
<thead>
<tr style="height: 13px;">
<th style="height: 13px; width: 406px;">Подсистема и требования</th>
<th style="height: 13px; width: 18px;" align="center">1Д</th>
<th style="height: 13px; width: 15px;" align="center">1Г</th>
<th style="height: 13px; width: 17px;" align="center">1В</th>
<th style="height: 13px; width: 17px;" align="center">1Б</th>
<th style="height: 13px; width: 17px;" align="center">1А</th>
<th style="height: 13px; width: 47px;" align="center">Lanbilling</th>
<th style="height: 13px; width: 439px;">Описание или решение</th>
</tr>
</thead>
<tbody>
<tr style="height: 13px;">
<td style="height: 13px; width: 406px;">1. Подсистема управления доступом</td>
<td style="height: 13px; width: 18px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 15px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 17px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 17px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 17px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 47px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 439px;">&nbsp;</td>
</tr>
<tr style="height: 26px;">
<td style="height: 26px; width: 406px;">1.1. Идентификация, проверка подлинности и контроль доступа субъектов:</td>
<td style="height: 26px; width: 18px;" align="center">&nbsp;</td>
<td style="height: 26px; width: 15px;" align="center">&nbsp;</td>
<td style="height: 26px; width: 17px;" align="center">&nbsp;</td>
<td style="height: 26px; width: 17px;" align="center">&nbsp;</td>
<td style="height: 26px; width: 17px;" align="center">&nbsp;</td>
<td style="height: 26px; width: 47px;" align="center">&nbsp;</td>
<td style="height: 26px; width: 439px;">&nbsp;</td>
</tr>
<tr style="height: 13px;">
<td style="height: 13px; width: 406px;">в систему</td>
<td style="height: 13px; width: 18px;" align="center">+</td>
<td style="height: 13px; width: 15px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 47px;" align="center">+</td>
<td style="height: 13px; width: 439px;">Авторизация по логину и паролю</td>
</tr>
<tr style="height: 26px;">
<td style="height: 26px; width: 406px;">к терминалам, ЭВМ, узлам сети ЭВМ, каналам связи, внешним устройствам ЭВМ</td>
<td style="height: 26px; width: 18px;" align="center">-</td>
<td style="height: 26px; width: 15px;" align="center">+</td>
<td style="height: 26px; width: 17px;" align="center">+</td>
<td style="height: 26px; width: 17px;" align="center">+</td>
<td style="height: 26px; width: 17px;" align="center">+</td>
<td style="height: 26px; width: 47px;" align="center">-</td>
<td style="height: 26px; width: 439px;"></td>
</tr>
<tr style="height: 13px;">
<td style="height: 13px; width: 406px;">к программам</td>
<td style="height: 13px; width: 18px;" align="center">-</td>
<td style="height: 13px; width: 15px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 47px;" align="center">-</td>
<td style="height: 13px; width: 439px;">нет доступа к другим программам</td>
</tr>
<tr style="height: 26px;">
<td style="height: 26px; width: 406px;">к томам, каталогам, файлам, записям, полям записей</td>
<td style="height: 26px; width: 18px;" align="center">-</td>
<td style="height: 26px; width: 15px;" align="center">+</td>
<td style="height: 26px; width: 17px;" align="center">+</td>
<td style="height: 26px; width: 17px;" align="center">+</td>
<td style="height: 26px; width: 17px;" align="center">+</td>
<td style="height: 26px; width: 47px;" align="center">-</td>
<td style="height: 26px; width: 439px;">нет доступа</td>
</tr>
<tr style="height: 27px;">
<td style="height: 27px; width: 406px;">1.2. Управление потоками информации</td>
<td style="height: 27px; width: 18px;" align="center">-</td>
<td style="height: 27px; width: 15px;" align="center">-</td>
<td style="height: 27px; width: 17px;" align="center">+</td>
<td style="height: 27px; width: 17px;" align="center">+</td>
<td style="height: 27px; width: 17px;" align="center">+</td>
<td style="height: 27px; width: 47px;" align="center">-</td>
<td style="height: 27px; width: 439px;">Имеется</td>
</tr>
<tr style="height: 13px;">
<td style="height: 13px; width: 406px;">2. Подсистема регистрации и учета</td>
<td style="height: 13px; width: 18px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 15px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 17px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 17px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 17px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 47px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 439px;">&nbsp;</td>
</tr>
<tr style="height: 13px;">
<td style="height: 13px; width: 406px;">2.1. Регистрация и учет:</td>
<td style="height: 13px; width: 18px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 15px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 17px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 17px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 17px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 47px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 439px;">&nbsp;</td>
</tr>
<tr style="height: 13px;">
<td style="height: 13px; width: 406px;">входа (выхода) субъектов доступа в (из) систему (узел сети)</td>
<td style="height: 13px; width: 18px;" align="center">+</td>
<td style="height: 13px; width: 15px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 47px;" align="center">+</td>
<td style="height: 13px; width: 439px;">Ведется запись входа/выхода всех пользователей</td>
</tr>
<tr style="height: 26px;">
<td style="height: 26px; width: 406px;">выдачи печатных (графических) выходных документов</td>
<td style="height: 26px; width: 18px;" align="center">-</td>
<td style="height: 26px; width: 15px;" align="center">+</td>
<td style="height: 26px; width: 17px;" align="center">+</td>
<td style="height: 26px; width: 17px;" align="center">+</td>
<td style="height: 26px; width: 17px;" align="center">+</td>
<td style="height: 26px; width: 47px;" align="center">+</td>
<td style="height: 26px; width: 439px;">Формирование отчетов о состоянии системы и т.д.</td>
</tr>
<tr style="height: 26px;">
<td style="height: 26px; width: 406px;">запуска (завершения) программ и процессов (заданий, задач)</td>
<td style="height: 26px; width: 18px;" align="center">-</td>
<td style="height: 26px; width: 15px;" align="center">+</td>
<td style="height: 26px; width: 17px;" align="center">+</td>
<td style="height: 26px; width: 17px;" align="center">+</td>
<td style="height: 26px; width: 17px;" align="center">+</td>
<td style="height: 26px; width: 47px;" align="center">+</td>
<td style="height: 26px; width: 439px;">Планирование отключения пользователей, выполнение копии БД</td>
</tr>
<tr style="height: 39px;">
<td style="height: 39px; width: 406px;">доступа программ субъектов доступа к защищаемым файлам, включая их создание и удаление, передачу по линиям и каналам связи</td>
<td style="height: 39px; width: 18px;" align="center">-</td>
<td style="height: 39px; width: 15px;" align="center">+</td>
<td style="height: 39px; width: 17px;" align="center">+</td>
<td style="height: 39px; width: 17px;" align="center">+</td>
<td style="height: 39px; width: 17px;" align="center">+</td>
<td style="height: 39px; width: 47px;" align="center">+</td>
<td style="height: 39px; width: 439px;">&nbsp;</td>
</tr>
<tr style="height: 39px;">
<td style="height: 39px; width: 406px;">доступа программ субъектов доступа к терминалам, ЭВМ, узлам сети ЭВМ, каналам связи, внешним устройствам ЭВМ, программам, томам, каталогам, файлам, записям, полям записей</td>
<td style="height: 39px; width: 18px;" align="center">-</td>
<td style="height: 39px; width: 15px;" align="center">+</td>
<td style="height: 39px; width: 17px;" align="center">+</td>
<td style="height: 39px; width: 17px;" align="center">+</td>
<td style="height: 39px; width: 17px;" align="center">+</td>
<td style="height: 39px; width: 47px;" align="center">-</td>
<td style="height: 39px; width: 439px;">Ограничение доступа до разрешенных администратором системы</td>
</tr>
<tr style="height: 13px;">
<td style="height: 13px; width: 406px;">изменения полномочий субъектов доступа</td>
<td style="height: 13px; width: 18px;" align="center">-</td>
<td style="height: 13px; width: 15px;" align="center">-</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 47px;" align="center">+</td>
<td style="height: 13px; width: 439px;"></td>
</tr>
<tr style="height: 13px;">
<td style="height: 13px; width: 406px;">создаваемых защищаемых объектов доступа</td>
<td style="height: 13px; width: 18px;" align="center">-</td>
<td style="height: 13px; width: 15px;" align="center">-</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 47px;" align="center">+</td>
<td style="height: 13px; width: 439px;">Пароль</td>
</tr>
<tr style="height: 13px;">
<td style="height: 13px; width: 406px;">2.2. Учет носителей информации</td>
<td style="height: 13px; width: 18px;" align="center">+</td>
<td style="height: 13px; width: 15px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 47px;" align="center">+</td>
<td style="height: 13px; width: 439px;">Подучетные сервера</td>
</tr>
<tr style="height: 26px;">
<td style="height: 26px; width: 406px;">2.3. Очистка (обнуление, обезличивание) освобождаемых областей оперативной памяти ЭВМ и внешних накопителей</td>
<td style="height: 26px; width: 18px;" align="center">-</td>
<td style="height: 26px; width: 15px;" align="center">+</td>
<td style="height: 26px; width: 17px;" align="center">+</td>
<td style="height: 26px; width: 17px;" align="center">+</td>
<td style="height: 26px; width: 17px;" align="center">+</td>
<td style="height: 26px; width: 47px;" align="center">+</td>
<td style="height: 26px; width: 439px;">Чистка кэша и cookie после выхода</td>
</tr>
<tr style="height: 13px;">
<td style="height: 13px; width: 406px;">2.4. Сигнализация попыток нарушения защиты</td>
<td style="height: 13px; width: 18px;" align="center">-</td>
<td style="height: 13px; width: 15px;" align="center">-</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 47px;" align="center">+</td>
<td style="height: 13px; width: 439px;"></td>
</tr>
<tr style="height: 13px;">
<td style="height: 13px; width: 406px;">3. Криптографическая подсистема</td>
<td style="height: 13px; width: 18px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 15px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 17px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 17px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 17px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 47px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 439px;">&nbsp;</td>
</tr>
<tr style="height: 13px;">
<td style="height: 13px; width: 406px;">3.1. Шифрование конфиденциальной информации</td>
<td style="height: 13px; width: 18px;" align="center">-</td>
<td style="height: 13px; width: 15px;" align="center">-</td>
<td style="height: 13px; width: 17px;" align="center">-</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 47px;" align="center">+</td>
<td style="height: 13px; width: 439px;">Данные хранятся в зашифрованным виде</td>
</tr>
<tr style="height: 26px;">
<td style="height: 26px; width: 406px;">3.2. Шифрование информации, принадлежащей различным субъектам доступа (группам субъектов) на разных ключах</td>
<td style="height: 26px; width: 18px;" align="center">-</td>
<td style="height: 26px; width: 15px;" align="center">-</td>
<td style="height: 26px; width: 17px;" align="center">-</td>
<td style="height: 26px; width: 17px;" align="center">-</td>
<td style="height: 26px; width: 17px;" align="center">+</td>
<td style="height: 26px; width: 47px;" align="center">-</td>
<td style="height: 26px; width: 439px;"></td>
</tr>
<tr style="height: 26px;">
<td style="height: 26px; width: 406px;">3.3. Использование аттестованных (сертифицированных) криптографических средств</td>
<td style="height: 26px; width: 18px;" align="center">-</td>
<td style="height: 26px; width: 15px;" align="center">-</td>
<td style="height: 26px; width: 17px;" align="center">-</td>
<td style="height: 26px; width: 17px;" align="center">+</td>
<td style="height: 26px; width: 17px;" align="center">+</td>
<td style="height: 26px; width: 47px;" align="center">+</td>
<td style="height: 26px; width: 439px;"></td>
</tr>
<tr style="height: 13px;">
<td style="height: 13px; width: 406px;">4. Подсистема обеспечения целостности</td>
<td style="height: 13px; width: 18px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 15px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 17px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 17px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 17px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 47px;" align="center">&nbsp;</td>
<td style="height: 13px; width: 439px;">&nbsp;</td>
</tr>
<tr style="height: 26px;">
<td style="height: 26px; width: 406px;">4.1. Обеспечение целостности программных средств и обрабатываемой информации</td>
<td style="height: 26px; width: 18px;" align="center">+</td>
<td style="height: 26px; width: 15px;" align="center">+</td>
<td style="height: 26px; width: 17px;" align="center">+</td>
<td style="height: 26px; width: 17px;" align="center">+</td>
<td style="height: 26px; width: 17px;" align="center">+</td>
<td style="height: 26px; width: 47px;" align="center">+</td>
<td style="height: 26px; width: 439px;">&nbsp;</td>
</tr>
<tr style="height: 26px;">
<td style="height: 26px; width: 406px;">4.2. Физическая охрана средств вычислительной техники и носителей информации</td>
<td style="height: 26px; width: 18px;" align="center">+</td>
<td style="height: 26px; width: 15px;" align="center">+</td>
<td style="height: 26px; width: 17px;" align="center">+</td>
<td style="height: 26px; width: 17px;" align="center">+</td>
<td style="height: 26px; width: 17px;" align="center">+</td>
<td style="height: 26px; width: 47px;" align="center">+</td>
<td style="height: 26px; width: 439px;">Охраняется</td>
</tr>
<tr style="height: 13px;">
<td style="height: 13px; width: 406px;">4.3. Наличие администратора (службы) защиты информации в АС</td>
<td style="height: 13px; width: 18px;" align="center">-</td>
<td style="height: 13px; width: 15px;" align="center">-</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 47px;" align="center">+</td>
<td style="height: 13px; width: 439px;"></td>
</tr>
<tr style="height: 13px;">
<td style="height: 13px; width: 406px;">4.4. Периодическое тестирование СЗИ НСД</td>
<td style="height: 13px; width: 18px;" align="center">+</td>
<td style="height: 13px; width: 15px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 47px;" align="center">+</td>
<td style="height: 13px; width: 439px;"></td>
</tr>
<tr style="height: 13px;">
<td style="height: 13px; width: 406px;">4.5. Наличие средств восстановления СЗИ НСД</td>
<td style="height: 13px; width: 18px;" align="center">+</td>
<td style="height: 13px; width: 15px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 47px;" align="center">+</td>
<td style="height: 13px; width: 439px;">Хранение данных на серверах в течении 3 лет с момента записи</td>
</tr>
<tr style="height: 13px;">
<td style="height: 13px; width: 406px;">4.6. Использование сертифицированных средств защиты</td>
<td style="height: 13px; width: 18px;" align="center">-</td>
<td style="height: 13px; width: 15px;" align="center">-</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 17px;" align="center">+</td>
<td style="height: 13px; width: 47px;" align="center">+</td>
<td style="height: 13px; width: 439px;"><br /></td>
</tr>
</tbody>
</table>
